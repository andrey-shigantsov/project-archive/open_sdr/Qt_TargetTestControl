#ifndef PROTOREADER_H
#define PROTOREADER_H

#include <QObject>
#include <QByteArray>

class ProtoReader : public QObject
{
  Q_OBJECT
public:
  explicit ProtoReader(QObject * parent = nullptr);

  void reset(){set_mode(false);}

  void read(quint8 byte);
  void read(QByteArray buf);

signals:
  void buf_ready(QByteArray buf);

private:
  bool isContent, isSyncLost;
  quint16 content_len;

  QByteArray buf;

  void set_mode(bool isContent)
  {
    isSyncLost = false;
    this->isContent = isContent;
    buf.clear();
  }
};

#endif // PROTOREADER_H
