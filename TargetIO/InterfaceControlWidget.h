#ifndef INTERFACECONTROLWIDGET_H
#define INTERFACECONTROLWIDGET_H

#include <QWidget>

namespace Ui {
class InterfaceControlWidget;
}

class InterfaceControlWidget : public QWidget
{
  Q_OBJECT

public:
  explicit InterfaceControlWidget(QWidget *parent = 0);
  ~InterfaceControlWidget();

  enum Interface {UnknownInterface = -1, COM, TCP};

  void setInterface(QString val);
  void setInterface(Interface val);

  void setWaitingState(bool flag);
  void setConnectionState(bool flag);

  void setComPort(QString name);
  void setComBaudRate(QString rate);
  void setComBaudRate(qint32 rate);
  QString comPort();
  qint32 comBaudRate();

  void setTcpHost(QString name);
  void setTcpPort(QString port);
  QString tcpHost();
  quint16 tcpPort();

signals:
  void connectionControlClicked();
  void interfaceChanged(InterfaceControlWidget::Interface val);

private:
  Ui::InterfaceControlWidget *ui;

  void ui_refresh_comPortIdx(int idx);
  void ui_refresh_comBaudRateIdx(int idx);
  void ui_setConnectionState(bool flag);

private slots:
  void ui_init_InerfaceList();
  void ui_refresh_comPortsList();
  void ui_refresh_comBaudRatesList();
  void on_Interface_currentIndexChanged(int index);
  void on_ConnectionControl_clicked();
};

#endif // INTERFACECONTROLWIDGET_H
