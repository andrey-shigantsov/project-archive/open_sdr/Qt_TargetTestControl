#include "TcpDevice.h"

#include <QHostInfo>

#include <SDR/target_interface/base/proto.h>

TcpDevice::TcpDevice(QObject *parent) :
  IODevice(parent)
{
  connect(&tcpSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(tcpsocket_statehandler(QAbstractSocket::SocketState)));
  connect(&tcpSocket, SIGNAL(readyRead()), this, SLOT(tcpsocket_read()));
  connect(&protoReader, SIGNAL(buf_ready(QByteArray)), this, SLOT(proto_buf_handler(QByteArray)));
}

bool TcpDevice::isOpen()
{
  return tcpSocket.isOpen();
}

QString TcpDevice::errorString()
{
  return tcpSocket.errorString();
}

void TcpDevice::setHost(QString hostname, quint16 port)
{
#ifdef SDR_TARGET_TEST_APP_TCP_FIND_HOSTADDR
  QHostInfo info = QHostInfo::fromName(hostname);
  if (info.error() != QHostInfo::NoError)
    qDebug() << metaObject()->className() << ": QHostInfo error: " << info.errorString();
  if (info.addresses().size())
    hostaddr = info.addresses().first();
#else
  this->hostname = hostname;
#endif
  this->port = port;
}

bool TcpDevice::open()
{
  tcpSocket.abort();
#ifdef SDR_TARGET_TEST_APP_TCP_FIND_HOSTADDR
  if (hostaddr.isNull())
    qDebug() << metaObject()->className() << ": hostAddr isNull";
  tcpSocket.connectToHost(hostaddr, port);
#else
  tcpSocket.connectToHost(hostname, port);
#endif
  return true;
}

void TcpDevice::close()
{
  tcpSocket.close();
}

qint64 TcpDevice::send(const char *data, qint64 size)
{
  char headerBuf[SDR_PROTO_HEADER_BUF_SIZE];
  proto_raw_write_header(size,(UInt8_t*)headerBuf,SDR_PROTO_HEADER_BUF_SIZE);

  QByteArray protoBuf = QByteArray::fromRawData(data, size);
  protoBuf.prepend(headerBuf, SDR_PROTO_HEADER_BUF_SIZE);
  protoBuf.append(SDR_PROTO_FINISH_SYM);

  tcpSocket.write(protoBuf);
  return size;
}

void TcpDevice::tcpsocket_statehandler(QAbstractSocket::SocketState state)
{
  switch(state)
  {
  case QAbstractSocket::UnconnectedState:{
    emit clossed();
    QTcpSocket::SocketError err = tcpSocket.error();
    if (err != QTcpSocket::UnknownSocketError)
      qDebug() << metaObject()->className() << ": clossed" << ": " << errorString();
    break;}
  case QAbstractSocket::ConnectedState:
    protoReader.reset();
    emit openned();
    break;
  default:
    break;
  }
}

void TcpDevice::tcpsocket_read()
{
  protoReader.read(tcpSocket.readAll());
}

void TcpDevice::proto_buf_handler(QByteArray buf)
{
  emit rx_buf_ready(buf);
}
