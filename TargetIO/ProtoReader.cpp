#include "ProtoReader.h"

#include <QDebug>

#include <SDR/target_interface/base/proto.h>

ProtoReader::ProtoReader(QObject *parent) :
  QObject(parent)
{
  set_mode(false);
}

void ProtoReader::read(quint8 byte)
{
  if(isContent)
  {
    if (buf.size() < content_len)
      buf.append(byte);
    else
    {
      if (byte == SDR_PROTO_FINISH_SYM)
        emit buf_ready(buf);
      else
        qWarning() << metaObject()->className() << "finish symbol invalid";
      set_mode(false);
    }

    if (buf.size() >= SDR_PROTO_HEADER_BUF_SIZE)
    {
      int idx = buf.size()-SDR_PROTO_HEADER_BUF_SIZE;
      if ((buf.at(idx) == SDR_PROTO_START_SYM))
      {
        bool crcOk = false;
        uint16_t content_len;
        bool res = proto_raw_read_header((unsigned char *)buf.data()+idx, SDR_PROTO_HEADER_BUF_SIZE, &content_len, &crcOk);
        if (res && crcOk)
        {
          qWarning() << metaObject()->className() << "reset: valid header found";
          this->content_len = content_len;
          set_mode(true);
          return;
        }
      }
    }
  }
  else
  {
    if ((buf.size() == 0)&&((uint8_t)byte != SDR_PROTO_START_SYM))
    {
      if (!isSyncLost)
      {
        isSyncLost = true;
        qWarning() << metaObject()->className() << "rx buf handler: proto sync lost";
      }
      return;
    }

    Q_ASSERT(buf.size() < SDR_PROTO_HEADER_BUF_SIZE);
    buf.append(byte);
    if (buf.size() == SDR_PROTO_HEADER_BUF_SIZE)
    {
      bool crcOk = false;
      bool res = proto_raw_read_header((UInt8_t *)buf.data(), buf.size(), &content_len, &crcOk);
      if (res && crcOk)
        set_mode(true);
      else
      {
        int newStartIdx = buf.indexOf(SDR_PROTO_START_SYM, 1);
        if (newStartIdx > 0)
          buf.remove(0,newStartIdx);
        else
          buf.clear();
        qWarning() << metaObject()->className() << ": " << "rx buf handler: header crc falure";
      }
    }
  }
}

void ProtoReader::read(QByteArray buf)
{
  for (quint64 i = 0; i < buf.size(); ++i)
    read(buf.at(i));
}
