#ifndef SERIALPORTDEVICE_H
#define SERIALPORTDEVICE_H

#include "IODevice.h"
#include "ProtoReader.h"

#include <QSerialPort>

class SerialPortDevice : public IODevice
{
  Q_OBJECT
public:
  explicit SerialPortDevice(QObject *parent = nullptr);

  QSerialPort * device(){return &port;}

  bool isOpen(){return port.isOpen();}

  qint64 send(const char *data, qint64 size);

  QString errorString();

public slots:
  bool open();
  void close();

  void setPortName(QString name);
  void setBaudRate(qint32 rate);

private:
  QSerialPort port;
  ProtoReader protoReader;

private slots:
  void OpennedHandler();
  void ClossedHandler();
  void ReadHandler();
  void protoBufHandler(QByteArray buf);
};

#endif // SERIALPORTDEVICE_H
