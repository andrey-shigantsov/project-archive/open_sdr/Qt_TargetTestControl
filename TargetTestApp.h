#ifndef TARGETTESTAPP_H
#define TARGETTESTAPP_H

#include <AbstractTestApp.h>
#include <SDR/Qt_Addons/Controls/TControlWidget.h>
#include <SDR/Qt_Addons/Controls/TStatusWidget.h>

#include <SDR/target_interface/io.h>
#include "TargetIO/InterfaceControlWidget.h"
#include "TargetIO/SerialPortDevice.h"
#include "TargetIO/TcpDevice.h"

#include <QMap>

namespace SDR
{

class TargetTestApp : public AbstractTestApp
{
  Q_OBJECT
public:
  enum ConnectionState {Disconnected, Connected};

  TargetTestApp(int timeout_ms = 50, QString interfaceStr = "COM");
  ~TargetTestApp();

  InterfaceControlWidget * ioControlWidget() {return &ioCW;}

  IODevice * ioDevice(){return ioDevicePtr;}

  TControlWidget * TargetControlWidget() {return TargetCW;}
  void setTargetControlWidget(TControlWidget * cw, const QByteArray formatHash = QByteArray());

  TStatusWidget * TargetStatusWidget() {return TargetSW;}
  void setTargetStatusWidget(TStatusWidget *cw, const QByteArray formatHash = QByteArray());

  QMap<int, TPlot *> * PlotsById(){return &PlotsMap;}

  void setControlFormatHash(const QByteArray hash){cfHash = hash;}
  const QByteArray controlFormatHash(){return cfHash;}
  void setStatusFormatHash(const QByteArray hash){sfHash = hash;}
  const QByteArray statusFormatHash(){return sfHash;}
  void setPlotsFormatHash(const QByteArray hash){pfHash = hash;}
  const QByteArray plotsFormatHash(){return pfHash;}

  virtual void setTimeout(int ms){AbstractTestApp::setTimeout(ms);sendParams();}

public slots:
  void sendParams();
  void sendUserControlValues();
  void getAll();
  void getFormat();
  void getState();

  void init();
  void start();
  void pause();
  void stop();
  void step();
  void reset();

private:  
  InterfaceControlWidget ioCW;
  TARGET_IO_t io;
  SerialPortDevice comPort;
  TcpDevice tcpSock;
  IODevice * ioDevicePtr;

  TControlWidget * TargetCW;
  TStatusWidget * TargetSW;
  QMap<int, TPlot *> PlotsMap;

  QByteArray cfHash, sfHash, pfHash;

  void refreshInterfaceParams();

  void disconnectInterface();

private slots:
  void ConnectionControlHandler();
  void OpennedHandler();
  void rxBufHandler(const QByteArray buf);
  void ClossedHandler();

  void sendValue(const int id, const QString &val);

  void refreshInterface(InterfaceControlWidget::Interface);
  void ui_setConnectionState(ConnectionState state);
};

}

#endif // TARGETTESTAPP_H
