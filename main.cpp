/*
 * main.cpp
 */

#include <QDebug>
#include <QFile>
#include <QDateTime>
#include <QApplication>
#include <QSettings>

#include "TargetTestApp.h"

static void init_log_message_handler();
static void parse_settings(QApplication * app, SDR::TargetTestApp * testApp);
int main(int argc, char *argv[])
{
    init_log_message_handler();

    QApplication a(argc, argv);
    SDR::TargetTestApp t;
    parse_settings(&a, &t);

    t.ControlWidget()->show();

    return a.exec();
}

static QFile logFile;
static void log_message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
static void init_log_message_handler()
{
  logFile.setFileName("TargetTestControl.log");
  qInstallMessageHandler(log_message_handler);

  QString entryMsg = "\n\n==={ OpenSDR TargetTestControl entry point }===\n";
  entryMsg += "\n  ";
  entryMsg += QDateTime::currentDateTime().toString("yyyy.MM.dd HH:mm:ss.zzz");
  entryMsg += "\n----------------------------------------------------------------\n";

  if (!logFile.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    fprintf(stderr, "[WRN] log file open failure: %s \n", logFile.errorString().toLocal8Bit().data());
    return;
  }
  logFile.write(entryMsg.toLocal8Bit());
  logFile.close();
}
static void log_message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString logStr;
    switch (type) {
    case QtDebugMsg:
        logStr = "[DBG] " + msg + "\n";
        break;
    case QtInfoMsg:
        logStr = msg + "\n";
        break;
    case QtWarningMsg:
        logStr = "[WRN] " + msg + "\n";
        break;
    case QtCriticalMsg:
        logStr = "!Critical! " + msg + QString::asprintf("%s:%u, %s)", context.file, context.line, context.function) + "\n";
        break;
    case QtFatalMsg:
        logStr = "!!Fatal!! " + msg + QString::asprintf("%s:%u, %s)", context.file, context.line, context.function) + "\n";
      break;
    }
    logStr.prepend(QDateTime::currentDateTime().toString("HH:mm:ss.zzz | "));

    if (!logFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
    {
      fprintf(stderr, "[WRN] log file open failure: %s \n", logFile.errorString().toLocal8Bit().data());
      fprintf(stderr, logStr.toLocal8Bit().data());
      return;
    }
    logFile.write(logStr.toLocal8Bit());
    logFile.close();

    switch (type) {
    case QtDebugMsg:
    case QtInfoMsg:
      fprintf(stdout, logStr.toLocal8Bit().data());
      break;
    case QtWarningMsg:
    case QtCriticalMsg:
    case QtFatalMsg:
      fprintf(stderr, logStr.toLocal8Bit().data());
      if (type == QtFatalMsg)
        abort();
      break;
    }
}

static void parse_settings(QSettings * settings, SDR::TargetTestApp * testApp)
{
  settings->beginGroup("Connection");{
    InterfaceControlWidget * ioCW = testApp->ioControlWidget();
    if (settings->contains("interface"))
      ioCW->setInterface(settings->value("interface").toString());
    if (settings->contains("tcpHost"))
      ioCW->setTcpHost(settings->value("tcpHost").toString());
    if (settings->contains("tcpPort"))
      ioCW->setTcpPort(settings->value("tcpPort").toString());
    if (settings->contains("comBaudRate"))
      ioCW->setComBaudRate(settings->value("comBaudRate").toString());
    if (settings->contains("comPort"))
      ioCW->setComPort(settings->value("comPort").toString());
  }settings->endGroup();
}
static void parse_settings(QSettings * settings, QApplication * app, SDR::TargetTestApp * testApp)
{
  Q_UNUSED(app);
  parse_settings(settings, testApp);
}
static void parse_settings(QApplication * app, SDR::TargetTestApp * testApp)
{
  QSettings userSettings(QSettings::IniFormat, QSettings::UserScope, "OpenSDR", "TargetTestControl");
  parse_settings(&userSettings, app, testApp);

  QSettings fileSettings("TargetTestControl.ini", QSettings::IniFormat);
  parse_settings(&fileSettings, app, testApp);
}
